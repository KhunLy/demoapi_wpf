﻿using Movies.DAL.Repositories;
using Movies.DAL.UOW;
using NetFlask.API.DTO.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToolBox.Mappers.Extensions;

namespace NetFlask.API.Services
{
    public class CategoryService : BaseService
    {
        public CategoryService(UnitOfWork uow) : base(uow)
        {
        }

        public IEnumerable<CategoryDTO> Get()
        {
            return _uow.Get<CategoryRepository>().Get()
                .Select(c => c.MapTo<CategoryDTO>());
        }
    }
}