﻿using Movies.DAL.Entities;
using Movies.DAL.Repositories;
using Movies.DAL.UOW;
using Movies.DAL.Views;
using NetFlask.API.DTO.Cast;
using NetFlask.API.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToolBox.Mappers.Extensions;

namespace NetFlask.API.Services
{
    public class MovieService : BaseService
    {
        public MovieService(UnitOfWork uow) : base(uow)
        {
        }

        public MovieIndexDTO GetIndex(int limit = 20, int offset = 0)
        {
            MovieIndexDTO dto = new MovieIndexDTO();
            dto.Count = _uow.Get<MovieRepository>().GetCount();
            dto.Limit = limit;
            dto.Offset = offset;
            dto.Movies = _uow.Get<MovieRepository>().Get(limit, offset)
                .Select(m => {
                    MovieDTO result = m.MapTo<MovieDTO>();
                    result.PosterUri = m.Poster != null ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Image/Movie/" + m.Id : null;
                    return result;
                });
            return dto;
        }

        public MovieDetailsDTO GetDetails(int id)
        {
            VMovieCategory m = _uow.Get<MovieRepository>().GetWithCategory(id);
            MovieDetailsDTO dto = m.MapTo<MovieDetailsDTO>();
            dto.PosterUri = m.Poster != null ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Image/Movie/" + m.Id : null;
            dto.Casting = _uow.Get<CastRepository>().GetCastingByMovie(id)
                .Select(c => c.MapTo<CastDetailsDTO>());
            return dto;
        }

        public IEnumerable<MovieDTO> GetByCategory(int id)
        {
            return _uow.Get<MovieRepository>().GetByCategory(id)
                .Select(m => {
                    MovieDTO result = m.MapTo<MovieDTO>();
                    result.PosterUri = m.Poster != null ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Image/Movie/" + m.Id : null;
                    return result;
                });
        }

        public int Insert(MovieFormDTO dto)
        {
            int id = _uow.Get<MovieRepository>().Insert(dto.MapTo<Movie>());
            _uow.Save();
            return id;
        }

        public bool Update(MovieFormDTO dto)
        {
            bool result = _uow.Get<MovieRepository>().Update(dto.MapTo<Movie>());
            _uow.Save();
            return result;
        }

        public bool Delete(int id)
        {
            bool result = _uow.Get<MovieRepository>().Delete(id);
            _uow.Save();
            return result;
        }

        public byte[] GetPoster(int id)
        {
            return _uow.Get<MovieRepository>().GetById(id)?.Poster;
        }
    }
}