﻿using Movies.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Ajax;

namespace NetFlask.API.Services
{
    public abstract class BaseService
    {
        protected readonly UnitOfWork _uow;
        public BaseService(UnitOfWork uow)
        {
            _uow = uow;
        }
    }
}