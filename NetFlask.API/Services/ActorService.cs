﻿using Movies.DAL.Repositories;
using Movies.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlask.API.Services
{
    public class ActorService : BaseService
    {
        public ActorService(UnitOfWork uow) : base(uow)
        {
        }

        public byte[] GetImage(int id)
        {
            return _uow.Get<ActorRepository>().GetById(id)?.Image;
        }
    }
}