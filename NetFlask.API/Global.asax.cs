using Microsoft.Extensions.DependencyInjection;
using Movies.DAL.UOW;
using NetFlask.API.App_Start;
using NetFlask.API.DI;
using NetFlask.API.Services;
using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NetFlask.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            DependencyResolver.SetResolver(new ServiceLocator(services.BuildServiceProvider()));
        }

        private void ConfigureServices(ServiceCollection services)
        {
            // Add services here
            services.AddTransient<UnitOfWork>();
            services.AddTransient<MovieService>();
            services.AddTransient<CategoryService>();
            services.AddTransient<ActorService>();
            services.AddTransient<CastService>();
            services.AddTransient<UserService>();
        }
    }
}
