﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlask.API.DTO.Movie
{
    public class MovieIndexDTO
    {
        public int Count { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public IEnumerable<MovieDTO> Movies { get; set; }
    }
}