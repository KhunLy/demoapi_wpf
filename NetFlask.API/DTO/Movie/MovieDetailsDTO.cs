﻿using NetFlask.API.DTO.Cast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlask.API.DTO.Movie
{
    public class MovieDetailsDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public int? ReleaseYear { get; set; }
        public string PosterUri { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public IEnumerable<CastDetailsDTO> Casting { get; set; }
    }
}