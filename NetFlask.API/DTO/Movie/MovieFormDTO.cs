﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlask.API.DTO.Movie
{
    public class MovieFormDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public int? ReleaseYear { get; set; }
        public byte[] Poster { get; set; }
        public int? CategoryId { get; set; }
    }
}