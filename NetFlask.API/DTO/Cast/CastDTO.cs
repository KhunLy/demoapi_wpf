﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlask.API.DTO.Cast
{
    public class CastDTO
    {
        public int MovieId { get; set; }
        public int ActorId { get; set; }
        public string CharacterName { get; set; }
    }
}