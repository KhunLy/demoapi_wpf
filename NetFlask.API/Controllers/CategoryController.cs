﻿using NetFlask.API.DTO.Category;
using NetFlask.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace NetFlask.API.Controllers
{
    public class CategoryController : ApiController
    {
        public IEnumerable<CategoryDTO> Get() 
        {
            return DependencyResolver.Current
                .GetService<CategoryService>()
                .Get();
        }
    }
}
