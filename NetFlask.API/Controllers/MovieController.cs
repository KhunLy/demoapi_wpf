﻿using Movies.DAL.Entities;
using NetFlask.API.DTO.Movie;
using NetFlask.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;

namespace NetFlask.API.Controllers
{
    public class MovieController : ApiController
    {
        public MovieIndexDTO Get([FromUri]int limit = 20, [FromUri]int offet = 0)
        {
            return DependencyResolver.Current.GetService<MovieService>().GetIndex(limit, offet);
        }

        [System.Web.Http.HttpGet()]
        [System.Web.Http.Route("api/Movie/Category/{id}")]
        public IEnumerable<MovieDTO> GetByCategory(int id)
        {
            return DependencyResolver.Current.GetService<MovieService>().GetByCategory(id);
        }

        public MovieDetailsDTO Get(int id)
        {
            return DependencyResolver.Current.GetService<MovieService>().GetDetails(id);
        }

        public int Post(MovieFormDTO dto)
        {
            return DependencyResolver.Current.GetService<MovieService>().Insert(dto);
        }

        public bool Put(MovieFormDTO dto)
        {
            return DependencyResolver.Current.GetService<MovieService>().Update(dto);
        }

        public bool Delete(int id)
        {
            return DependencyResolver.Current.GetService<MovieService>().Delete(id);
        }
    }
}
