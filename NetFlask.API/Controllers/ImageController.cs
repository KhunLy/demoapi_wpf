﻿using NetFlask.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace NetFlask.API.Controllers
{
    public class ImageController : Controller
    {
        // GET: Image
        public ActionResult Movie(int id)
        {
            byte[] poster = DependencyResolver.Current.GetService<MovieService>()
                .GetPoster(id);
            if(poster != null)
                return File(poster, "image/png");
            return new HttpNotFoundResult();
        }

        public ActionResult Actor(int id)
        {
            byte[] image = DependencyResolver.Current.GetService<ActorService>()
                .GetImage(id);
            if (image != null)
                return File(image, "image/png");
            return new HttpNotFoundResult();
        }
    }
}