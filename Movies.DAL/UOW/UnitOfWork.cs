﻿using Microsoft.Extensions.DependencyInjection;
using Movies.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Movies.DAL.UOW
{
    public class UnitOfWork: IDisposable
    {
        private readonly ServiceCollection _collection;
        private readonly ServiceProvider _provider;
        private TransactionScope _transaction;

        public UnitOfWork()
        {
            _collection = new ServiceCollection();
            ConfigureService();
            _provider = _collection.BuildServiceProvider();
            //_transaction = new TransactionScope(TransactionScopeOption.Required);
        }

        public TInterface Get<TInterface>()
        {
            return _provider.GetService<TInterface>();
        }

        private void ConfigureService()
        {
            // Add repositories here
            _collection.AddScoped<MovieRepository>();
            _collection.AddScoped<ActorRepository>();
            _collection.AddScoped<CastRepository>();
            _collection.AddScoped<CategoryRepository>();
            _collection.AddScoped<UserRepository>();
        }

        public bool Save()
        {
            //try
            //{
            //    _transaction.Complete();
            //    return true;
            //}
            //catch (Exception e)
            //{

            //    Debug.Write(e);
            //    return false;
            //}
            //finally
            //{
            //    _transaction = new TransactionScope(TransactionScopeOption.Required);
            //}
            return true;
        }

        #region Dispose Pattern
        private bool _disposed = false;

        protected void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                //_transaction.Dispose();
                _provider.Dispose();
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        } 
        #endregion
    }
}
