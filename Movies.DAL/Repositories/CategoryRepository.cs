﻿using Movies.DAL.Entities;
using Movies.DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Repositories
{
    public class CategoryRepository: BaseRepository
    {
        #region Constructors
        public CategoryRepository() : base() { }
        public CategoryRepository(string connectionString) : base(connectionString) { }
        #endregion

        #region Methods
        public IEnumerable<Category> Get()
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Category] ORDER BY [name]";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader.MapToCategory();
                }
            }
        }

        public Category GetById(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM [Category] WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    return reader.MapToCategory();
                }
                return null;
            }
        }

        public int Insert(Category c)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "INSERT INTO [Category]([name],[description]) " +
                    "OUTPUT INSERTED.id VALUES " +
                    "(@name, @description)";
                command.Parameters.AddWithValue("@name", c.Name);
                command.Parameters.AddWithValue("@description", (object)c.Description ?? DBNull.Value);
                return (int)command.ExecuteScalar();
            }
        }

        public bool Update(Category c)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE [Category] SET " +
                    "[name] = @name, " +
                    "[description] = @description " +
                    "WHERE id = @id";
                command.Parameters.AddWithValue("@name", c.Name);
                command.Parameters.AddWithValue("@description", (object)c.Description ?? DBNull.Value);
                command.Parameters.AddWithValue("@id", c.Id);
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool Delete(int id)
        {
            using (SqlConnection conn = GetConnection())
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "DELETE FROM [Category] WHERE id = @id";
                command.Parameters.AddWithValue("@id", id);
                return command.ExecuteNonQuery() == 1;
            }
        } 
        #endregion
    }
}
