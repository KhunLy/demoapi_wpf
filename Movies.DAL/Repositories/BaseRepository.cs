﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.DAL.Repositories
{
    public abstract class BaseRepository
    {
        private readonly string CONNECTION_STRING;
        public BaseRepository(string connectionString)
        {
            CONNECTION_STRING = connectionString;
        }
        public BaseRepository()
        {
            CONNECTION_STRING = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
        }
        protected SqlConnection GetConnection()
        {
            return new SqlConnection(CONNECTION_STRING);
        }
    }
}
