﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.Mappers.Extensions
{
    public static class MapperExtensions
    {
        public static TResult MapTo<TResult>(this object from, params object[] parameters)
        {
            Type[] ctorTypes = parameters.Select(p => p.GetType()).ToArray();
            var ctor = typeof(TResult).GetConstructor(ctorTypes);
            TResult result = (TResult)ctor.Invoke(parameters);
            if (ctor == null) throw new ArgumentException("Invalid Constructor parameters");
            foreach(var prop in typeof(TResult).GetProperties())
            {
                var prop2 = from.GetType().GetProperty(prop.Name);
                if (prop2 != null)
                {
                    try
                    {
                        prop.SetValue(result, prop2.GetValue(from));
                    }
                    catch(Exception e)
                    {
                        Debug.Write(e.Message);
                    }
                }
            }
            return result;
        }

        public static TResult MapTo<TResult>(this IDataReader reader)
            where TResult: new()
        {
            TResult result = new TResult();
            foreach (var prop in typeof(TResult).GetProperties())
            {
                try
                {
                    object value = reader[prop.Name] == DBNull.Value 
                        ? null : reader[prop.Name];
                    prop.SetValue(result, value);
                }
                catch(IndexOutOfRangeException e)
                {
                    Debug.Write("The property name doesn't match any column");
                    Debug.Write(e.Message);
                }
                catch(Exception e)
                {
                    Debug.Write(e.Message);
                }
            }
            return result;
        }
    }
}
