﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.MVVM.Mediators
{
    public class Messenger
    {
        private readonly Dictionary<string, object> _topics;

        public Messenger()
        {
            _topics = new Dictionary<string, object>();
        }

        public void Publish(string topic)
        {
            (_topics[topic] as Action)?.Invoke();
        }

        public void Publish<T>(string topic, T paramter)
        {
            (_topics[topic] as Action<T>)?.Invoke(paramter);
        }

        public void Subsribe(string topic, Action action)
        {
            if (action == null || topic == null) throw new ArgumentException();
            if (!_topics.ContainsKey(topic))
            {
                _topics[topic] = action;
            }
            else
            {
                Action a = _topics[topic] as Action;
                a += action;
            }
        }

        public void Unsubscribe(string topic, Action action)
        {
            if (action == null || topic == null) throw new ArgumentException();
            if (_topics.ContainsKey(topic))
            {
                Action a = _topics[topic] as Action; 
                a -= action;
            }
        }

        

        public void Subsribe<T>(string topic, Action<T> action)
        {
            if (action == null || topic == null) throw new ArgumentException();
            if (!_topics.ContainsKey(topic))
            {
                _topics[topic] = action;
            }
            else
            {
                Action<T> a = _topics[topic] as Action<T>; 
                a += action;
            }
        }

        public void Unsubscribe<T>(string topic, Action<T> action)
        {
            if (action == null || topic == null) throw new ArgumentException();
            if (_topics.ContainsKey(topic))
            {
                Action<T> a = _topics[topic] as Action<T>;
                a -= action;
            }
        }

    }
}
