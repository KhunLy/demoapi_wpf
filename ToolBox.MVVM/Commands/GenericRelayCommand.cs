﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToolBox.MVVM.Commands
{
    public class RelayCommand<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action<T> _execute;

        private readonly Func<bool> _canExecute;


        public RelayCommand(Action<T> execute, Func<bool> canExecute = null)
        {
            if (execute == null) throw new ArgumentException();
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public void Execute(object parameter)
        {
            if (!(parameter is T)) throw new ArgumentException();
            _execute?.Invoke((T)parameter);
        }

        public void RaiseCanExecuteChanged(EventArgs e = null)
        {
            CanExecuteChanged?.Invoke(this, e);
        }
    }
}
