﻿using NetFlask.DI;
using NetFlask.Models.Movie;
using NetFlask.Providers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;
using ToolBox.MVVM.Mediators;

namespace NetFlask.ViewModels
{
    class MovieIndexViewModel : BindableBase
    {
        private MovieProvider _movieProvider;

        private ObservableCollection<MovieModel> _movies;
        public ObservableCollection<MovieModel> Movies { get { return _movies; } set { SetValue(ref _movies, value); } }
        
        public MovieIndexViewModel()
        {
            _movieProvider = new MovieProvider();
            Movies = new ObservableCollection<MovieModel>(_movieProvider.Get().Movies);
            ServiceLocator.Instance.GetService<Messenger>().Subsribe("MovieIndexRefresh", Refresh);
        }

        public void Refresh()
        {
            Movies = new ObservableCollection<MovieModel>(_movieProvider.Get().Movies);
        }
    }
}
