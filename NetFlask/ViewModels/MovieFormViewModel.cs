﻿using NetFlask.DI;
using NetFlask.Models.Category;
using NetFlask.Models.Movie;
using NetFlask.Providers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.Mappers.Extensions;
using ToolBox.MVVM.BaseClasses;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediators;

namespace NetFlask.ViewModels
{
    class MovieFormViewModel : BindableBase
    {
        private MovieFormModel model;

        public MovieFormModel Model
        {
            get { return model; }
            set { SetValue(ref model, value); }
        }


        private ObservableCollection<CategoryModel> categories;

        public ObservableCollection<CategoryModel> Categories
        {
            get { return categories; }
            set { SetValue(ref categories, value); }
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { SetValue(ref fileName, value); }
        }

        private CategoryModel selectedCategory;

        public CategoryModel SelectedCategory
        {
            get { return selectedCategory; }
            set { SetValue(ref selectedCategory, value); }
        }

        public RelayCommand SaveCmd { get; set; }


        private MovieProvider _movieProvider;
        private CategoryProvider _categoryProvider;


        public MovieFormViewModel(int id = 0)
        {
            SaveCmd = new RelayCommand(Save);
            _movieProvider = new MovieProvider();
            _categoryProvider = new CategoryProvider();
            Categories = new ObservableCollection<CategoryModel>(_categoryProvider.Get());

            Model = new MovieFormModel();
            if (id != 0)
            {
                Model = _movieProvider.Get(id).MapTo<MovieFormModel>();
            }
        }

        private void Save()
        {
            if (SelectedCategory != null)
            {
                Model.CategoryId = SelectedCategory.Id;
            }
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                Model.Poster = File.ReadAllBytes(FileName);
            }
            if (Model.Id == 0)
            {
                if (_movieProvider.Insert(Model))
                {
                    ServiceLocator.Instance.GetService<Messenger>().Publish("MovieIndexRefresh");
                    ServiceLocator.Instance.GetService<Messenger>().Publish("MovieFormWindowClose");
                }
            }
            else
            {
                //_movieProvider.Update(Model);
            }
        }
    }
}
