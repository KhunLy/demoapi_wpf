﻿using NetFlask.Models.Category;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NetFlask.Providers
{
    public class CategoryProvider
    {
        private string _api = ConfigurationManager.AppSettings.Get("api");
        public IEnumerable<CategoryModel> Get()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(_api + "api/Category").Result;
                if (response.IsSuccessStatusCode)
                {
                    HttpContent content = response.Content;
                    string json = content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<CategoryModel>>(json);
                }
                return new List<CategoryModel>();
            }
        }
    }
}
