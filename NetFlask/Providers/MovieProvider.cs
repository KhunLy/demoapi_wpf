﻿using NetFlask.Models.Movie;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NetFlask.Providers
{
    class MovieProvider
    {
        private string _api = ConfigurationManager.AppSettings.Get("api");
        public MovieIndexModel Get()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(_api + "api/Movie").Result;
                if(response.IsSuccessStatusCode)
                {
                    HttpContent content = response.Content;
                    string json = content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<MovieIndexModel>(json);
                }
                return null;
            }
        }

        public MovieDetailsModel Get(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(_api + "api/Movie/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    HttpContent content = response.Content;
                    string json = content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<MovieDetailsModel>(json);
                }
                return null;
            }
        }

        public bool Insert(MovieFormModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                string json = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(_api + "api/Movie", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return true;
            }
        }
    }
}
