﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetFlask.Models.Movie
{
    class MovieIndexModel
    {
        public int Count { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
        public IEnumerable<MovieModel> Movies { get; set; }
    }
}
