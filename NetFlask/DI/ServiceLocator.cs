﻿using Microsoft.Extensions.DependencyInjection;
using NetFlask.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.Mediators;

namespace NetFlask.DI
{
    public class ServiceLocator
    {
        private static ServiceLocator _instance;

        public static ServiceLocator Instance
        {
            get
            {
                return _instance = _instance ?? new ServiceLocator();
            }
        }

        private ServiceProvider _provider;
        private ServiceCollection _collection;

        private ServiceLocator()
        {
            _collection = new ServiceCollection();
            ConfigureServices();
            _provider = _collection.BuildServiceProvider();
        }

        private void ConfigureServices()
        {
            _collection.AddSingleton<MovieProvider>();
            _collection.AddSingleton<CategoryProvider>();
            _collection.AddSingleton<Messenger>();
        }

        public T GetService<T>()
        {
            return _provider.GetService<T>();
        }

        public IEnumerable<object> GetServices<T>()
        {
            return _provider.GetServices(typeof(T));
        }
    }
}
