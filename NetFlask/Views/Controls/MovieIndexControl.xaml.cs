﻿using NetFlask.ViewModels;
using NetFlask.Views.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetFlask.Views.Controls
{
    /// <summary>
    /// Logique d'interaction pour MovieIndexControl.xaml
    /// </summary>
    public partial class MovieIndexControl : UserControl
    {
        public MovieIndexControl()
        {
            DataContext = new MovieIndexViewModel();
            InitializeComponent();
        }

        private void ShowDetails(object sender, RoutedEventArgs e)
        {

        }

        private void Add(object sender, RoutedEventArgs e)
        {
            MovieFormWindow window = new MovieFormWindow();
            window.Show();
        }
    }
}
