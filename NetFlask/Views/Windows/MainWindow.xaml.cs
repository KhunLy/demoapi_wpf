﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NetFlask.Views.Windows
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            foreach (Control item in MainContent.Children)
            {
                item.Visibility = Visibility.Collapsed;
                Button b = new Button { Content = item.Name };
                b.Click += (s, e) =>
                {
                    foreach(Control c in MainContent.Children)
                    {
                        c.Visibility = Visibility.Collapsed;
                    }
                    item.Visibility = Visibility.Visible;
                };
                Menu.Children.Add(b);
            }
        }
    }
}
