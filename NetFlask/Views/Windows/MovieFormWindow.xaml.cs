﻿using Microsoft.Win32;
using NetFlask.DI;
using NetFlask.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToolBox.MVVM.Mediators;

namespace NetFlask.Views.Windows
{
    /// <summary>
    /// Logique d'interaction pour MovieAddWindow.xaml
    /// </summary>
    public partial class MovieFormWindow : Window
    {
        public MovieFormWindow(int id = 0)
        {
            DataContext = new MovieFormViewModel(id);
            InitializeComponent();
            ServiceLocator.Instance.GetService<Messenger>()
                .Subsribe("MovieFormWindowClose", CloseWindow);
        }

        private void Browse(object sender, RoutedEventArgs e)
        {
            OpenFileDialog flg = new OpenFileDialog();
            flg.ShowDialog();
            PosterUri.Text = flg.FileName;
        }

        private void CloseWindow() 
        {
            Close();
        }
    }
}
